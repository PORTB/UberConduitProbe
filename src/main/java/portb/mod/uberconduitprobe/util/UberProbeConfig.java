package portb.mod.uberconduitprobe.util;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import portb.mod.uberconduitprobe.UberConduitProbe;


@Config(modid = UberConduitProbe.MODID)
@Mod.EventBusSubscriber(modid = UberConduitProbe.MODID)
public class UberProbeConfig
{
    @Config.RangeInt(min = 0, max = Integer.MAX_VALUE)
    @Config.Comment("The maximum amount of blocks that can be selected for configuration.")
    public static int MaxConfigurableVolume = 32768;

    @SubscribeEvent
    public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event)
    {
        if (event.getModID().equals(UberConduitProbe.MODID))
        {
            ConfigManager.sync(UberConduitProbe.MODID, Config.Type.INSTANCE);
        }
    }

/*    private static final int DEFAULT_MAX_CONFIGURABLE_VOLUME = 32768;
    private static final String CATEGORY_CORE = "core";
    private final Configuration config;

    public Config(FMLPreInitializationEvent event)
    {
        final File configFile = new File(event.getModConfigurationDirectory(), UberConduitProbe.MODID + ".cfg");
        config = new Configuration(configFile, "1.0.0");

        loadConfig();
    }

    private void loadConfig()
    {
        Property prop = config.get(CATEGORY_CORE, "maxConfigurableVolume", DEFAULT_MAX_CONFIGURABLE_VOLUME);
        prop.setName("Maximum configurable volume");
        prop.setComment("The maximum number of blocks that can be selected for configuration.");
        prop.setRequiresMcRestart(false);
        prop.setMaxValue(Integer.MAX_VALUE);
        prop.setMinValue(1);

        if(config.hasChanged())
            config.save();

    }

    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event)
    {
        if(UberConduitProbe.MODID.equals(event.getModID()))
        {

        }
    }*/
}
