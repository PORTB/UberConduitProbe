package portb.mod.uberconduitprobe.util;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Tuple;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import org.lwjgl.opengl.GL11;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

//Some of this code is from DireWolf20's building gadget mod
//If I understood how opengl and rendering stuff worked, I would probably
//not have needed to copy. Unfortunatley, due to school (we live in a
//society) I haven't had time to learn how it works.
public class RendererHelper
{

    final private static Cache<Tuple<BlockPos, BlockPos>, Integer> cacheOverlay =
            CacheBuilder
                    .newBuilder()
                    .maximumSize(1)
                    .expireAfterWrite(1, TimeUnit.SECONDS)
                    .removalListener(removal -> GLAllocation.deleteDisplayLists((int) removal.getValue()))
                    .build();

    public static void renderSelectedAreaOverlay(RenderWorldLastEvent event, EntityPlayer player, BlockPos firstPos, BlockPos secondPos, byte red, byte blue, byte green)
    {
        GlStateManager.pushMatrix();

        //Make the render be positioned regardless of the player's position
        double doubleX = player.lastTickPosX + (player.posX - player.lastTickPosX) * event.getPartialTicks();
        double doubleY = player.lastTickPosY + (player.posY - player.lastTickPosY) * event.getPartialTicks();
        double doubleZ = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * event.getPartialTicks();

        GlStateManager.translate(-doubleX, -doubleY, -doubleZ);

        try
        {
            //region
            // @formatter:off
            GlStateManager.callList
                    (

                            cacheOverlay.get
                                    (
                                            new Tuple<>
                                                    (
                                                            firstPos,
                                                            secondPos//,
                                                            //facing
                                                    ),
                                            () ->
                                            {
                                                int displayList = GLAllocation.generateDisplayLists(1);
                                                GlStateManager.glNewList(displayList, GL11.GL_COMPILE);
                                                render(player, player.world, firstPos, secondPos, red, blue, green);
                                                GlStateManager.glEndList();
                                                return displayList;
                                            }
                                    )
                    );
            // @formatter:on
            //endregion
        }
        catch (ExecutionException e)
        {
            throw new RuntimeException(e);
        }

        GlStateManager.enableLighting();
        GlStateManager.popMatrix();
    }

    private static void render(EntityPlayer player, World world, BlockPos start, BlockPos end, byte red, byte blue, byte green)
    {
        // We want to draw from the starting position to the (ending position)+1
        int x = Math.min(start.getX(), end.getX());
        int y = Math.min(start.getY(), end.getY());
        int z = Math.min(start.getZ(), end.getZ());

        int distanceX = Math.max(start.getX() + 1, end.getX() + 1);
        int distanceY = Math.max(start.getY() + 1, end.getY() + 1);
        int distanceZ = Math.max(start.getZ() + 1, end.getZ() + 1);

        Tessellator tessellator = Tessellator.getInstance();

        GlStateManager.pushMatrix();

        GlStateManager.disableLighting();
        GlStateManager.disableTexture2D();
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);

        //Make the shapes slightly bigger to avoid z-fighting
        renderSolidBox(tessellator, x - 0.006, y - 0.006, z - 0.006, distanceX + 0.006, distanceY + 0.006, distanceZ + 0.006, red / 255.0f, blue / 255.0f, green / 255.0f, 0.5f);
        renderBox(tessellator, x - 0.0011, y - 0.0011, z - 0.0011, distanceX + 0.0011, distanceY + 0.0011, distanceZ + 0.0011, red / 255.0f, blue / 255.0f, green / 255.0f, 0.75f);

        GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        ForgeHooksClient.setRenderLayer(MinecraftForgeClient.getRenderLayer());

        GlStateManager.enableTexture2D();
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
    }

    private static void renderBox(Tessellator tessellator, double startX, double startY, double startZ, double endX, double endY, double endZ, float red, float green, float blue, float alpha)
    {
        BufferBuilder bufferBuilder = tessellator.getBuffer();

        GlStateManager.glLineWidth(2.0F);
        bufferBuilder.begin(3, DefaultVertexFormats.POSITION_COLOR);
        bufferBuilder.pos(startX, startY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, startY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, startY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, startY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, startY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, startY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, endY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, endY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, endY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, endY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, endY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, endY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, startY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, startY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, endY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, endY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, startY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, startY, startZ).color(red, green, blue, alpha).endVertex();
        tessellator.draw();

        GlStateManager.glLineWidth(1.0F);
    }

    private static void renderSolidBox(Tessellator tessellator, double startX, double startY, double startZ, double endX, double endY, double endZ, float red, float green, float blue, float alpha)
    {
        BufferBuilder bufferBuilder = tessellator.getBuffer();

        bufferBuilder.begin(7, DefaultVertexFormats.POSITION_COLOR);

        //down
        bufferBuilder.pos(startX, startY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, startY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, startY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, startY, endZ).color(red, green, blue, alpha).endVertex();

        //up
        bufferBuilder.pos(startX, endY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, endY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, endY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, endY, startZ).color(red, green, blue, alpha).endVertex();

        //east
        bufferBuilder.pos(startX, startY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, endY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, endY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, startY, startZ).color(red, green, blue, alpha).endVertex();

        //west
        bufferBuilder.pos(startX, startY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, startY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, endY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, endY, endZ).color(red, green, blue, alpha).endVertex();

        //south
        bufferBuilder.pos(endX, startY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, endY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, endY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(endX, startY, endZ).color(red, green, blue, alpha).endVertex();

        //north
        bufferBuilder.pos(startX, startY, startZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, startY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, endY, endZ).color(red, green, blue, alpha).endVertex();
        bufferBuilder.pos(startX, endY, startZ).color(red, green, blue, alpha).endVertex();
        tessellator.draw();
    }

    //region unused code
    private static Comparator<BlockPos> getRelativePosComparator(Vec3d playerPos)
    {
        return (o1, o2) ->
        {
            Vec3d a = new Vec3d(o1.getX(), o1.getY(), o1.getZ());
            Vec3d b = new Vec3d(o2.getX(), o2.getY(), o2.getZ());

            double aDist = a.distanceTo(playerPos);
            double bDist = b.distanceTo(playerPos);

            if (aDist < bDist)
            {
                return -1;
            }
            else if (aDist == bDist)
            {
                return 0;
            }
            else if (aDist > bDist)
            {
                return 1;
            }

            throw new RuntimeException("This should never occur");
        };
    }

    private static void renderMyOverlay(EntityPlayer player, World world, BlockPos start, BlockPos end, byte red, byte blue, byte green)
    {
        //Vec3d playerPos = new Vec3d(player.posX, player.posY, player.posZ);
        //Set<BlockPos> coordinates = makeSetForBlockPosRange(start, end);

        GlStateManager.pushMatrix();
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);

        //coordinates = coordinates.stream().sorted(getRelativePosComparator(playerPos)).collect(Collectors.toCollection(LinkedHashSet::new));

        /*for(BlockPos pos : coordinates)
        {
            renderBlockPosOverlay(pos, red, green, blue);
        }*/

        renderBlockRangeOverlay(start, end, red, blue, green);

        GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        ForgeHooksClient.setRenderLayer(MinecraftForgeClient.getRenderLayer());

        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
    }

    //render a single, scaled up (made bigger) -, since the shape will *always* be a cuboid we don't have to bother rendering a box for each block.
    private static void renderBlockRangeOverlay(BlockPos start, BlockPos finish, byte red, byte blue, byte green)
    {
        Tessellator tessellator = Tessellator.getInstance();

        GlStateManager.pushMatrix();

        GlStateManager.translate(start.getX(), start.getY(), start.getZ()); //translate to the position we want

        GlStateManager.rotate(0, 0.0F, 1f, 0.0F); //rotate it??? idk why
        GlStateManager.translate(-0.005f, -0.005f, -0.005f); //translate it outwards a bit to be slightly outside the block
        GlStateManager.scale((finish.getX() - start.getX()) + 0.01f, (finish.getY() - start.getY()) + 0.01f, (finish.getZ() - start.getZ()) + 0.01f); //make the block slightly larger to prevent z fighting

        GlStateManager.disableLighting();
        GlStateManager.disableTexture2D();

        renderSolidBox(tessellator, 0, 0, 0, 1, 1, -1, red / 255.0f, blue / 255.0f, green / 255.0f, 0.5f);

        GlStateManager.enableTexture2D();
        GlStateManager.enableLighting();
        GlStateManager.popMatrix();
    }

    private static void renderBlockPosOverlay(BlockPos pos, byte red, byte green, byte blue)
    {
        Tessellator tessellator = Tessellator.getInstance();

        GlStateManager.pushMatrix();
        GlStateManager.translate(pos.getX(), pos.getY(), pos.getZ()); //translate to the position we want
        GlStateManager.rotate(-90.0F, 0.0F, 1.0F, 0.0F); //rotate it??? idk why
        GlStateManager.translate(-0.005f, -0.005f, 0.005f); //translate it outwards a bit to be slightly outside the block
        GlStateManager.scale(1.01f, 1.01f, 1.01f); //make the block slightly larger to prevent z fighting

        GlStateManager.disableLighting();
        GlStateManager.disableTexture2D();

        renderSolidBox(tessellator, 0, 0, -1, 1, 1, 0, red / 255.0f, blue / 255.0f, green / 255.0f, 0.5f);

        GlStateManager.enableTexture2D();
        GlStateManager.enableLighting();
        GlStateManager.popMatrix();
    }


    static private Set<BlockPos> makeSetForBlockPosRange(BlockPos start, BlockPos end)
    {
        List<BlockPos> posList = new ArrayList<>();

        for (int x = Math.min(start.getX(), end.getX()); x <= Math.max(start.getX(), end.getX()); x++)
        {
            for (int y = Math.min(start.getY(), end.getY()); y <= Math.max(start.getY(), end.getY()); y++)
            {
                for (int z = Math.min(start.getZ(), end.getZ()); z <= Math.max(start.getZ(), end.getZ()); z++)
                {
                    posList.add(new BlockPos(x, y, z));
                }
            }
        }

        return new HashSet<>(posList);
    }
//endregion
}
