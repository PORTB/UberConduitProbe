package portb.mod.uberconduitprobe.item;

import com.enderio.core.client.handlers.SpecialTooltipHandler;
import crazypants.enderio.api.tool.IHideFacades;
import crazypants.enderio.base.conduit.IConduitBundle;
import crazypants.enderio.base.conduit.IServerConduit;
import crazypants.enderio.base.conduit.RaytraceResult;
import crazypants.enderio.base.conduit.geom.CollidableComponent;
import crazypants.enderio.base.handler.KeyTracker;
import crazypants.enderio.conduits.conduit.BlockConduitBundle;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import org.lwjgl.input.Keyboard;
import portb.mod.uberconduitprobe.UberConduitProbe;
import portb.mod.uberconduitprobe.util.UberProbeConfig;
import portb.mod.uberconduitprobe.util.VectorHelper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.logging.Logger;

public class UberProbeItem extends Item implements IHideFacades
{
    public static final String REGISTRY_NAME = "uber_conduit_probe_item";
    private final static String NBT_FIRST_POS_KEY = "first_pos";
    private final static String NBT_SECOND_POS_KEY = "second_pos";
    private final static String NBT_CONDUIT_DATA_KEY = "conduit_data";
    private final static String NBT_CONDUIT_DIRECTION_KEY = "direction";
    private final static String NBT_SELECTING_KEY = "is_selecting";
    private static final Logger logger = Logger.getLogger("UberProbeItem");

    private static int getMaxConfigurationVolume()
    {
        return UberProbeConfig.MaxConfigurableVolume;
    }

    public UberProbeItem()
    {
        setRegistryName(new ResourceLocation(UberConduitProbe.MODID, REGISTRY_NAME));
        setUnlocalizedName(UberConduitProbe.MODID + "." + REGISTRY_NAME);
        setHasSubtypes(true);
        setMaxStackSize(1);
        setCreativeTab(CreativeTabs.TOOLS);
    }

    public static ItemMode getItemMode(ItemStack stack)
    {
        if (!(stack.getItem() instanceof UberProbeItem))
            throw new IllegalArgumentException("Cannot get mode for item " + stack.getItem().getClass().getName());

        //this line is stupid
        //get the item from the stack and then use the stack of the item to get the damage of the stack
        if (stack.getItem().getDamage(stack) == 0)
            return ItemMode.COPY;
        else
            return ItemMode.PASTE;
    }

    public static boolean isSelecting(ItemStack stack)
    {
        if (stack.getTagCompound() == null)
            return false;
        return stack.getTagCompound().hasKey(NBT_FIRST_POS_KEY);
    }

    public static BlockPos getFirstPosition(ItemStack stack)
    {
        if (stack.getTagCompound() == null)
            throw new IllegalArgumentException("Stack had no " + NBT_FIRST_POS_KEY + " tag");

        return BlockPos.fromLong(stack.getTagCompound().getLong(NBT_FIRST_POS_KEY));
    }

    public static boolean isShiftDown()
    {
        return Keyboard.isKeyDown(42) || Keyboard.isKeyDown(54);
    }

    @Override
    @ParametersAreNonnullByDefault
    @MethodsReturnNonnullByDefault
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand)
    {
        if (!world.isRemote)
        {
            RayTraceResult lookingAt = VectorHelper.getPlayerLookingAt(player);

            //if the player is looking at nothing
            if (lookingAt == null)
            {
                resetSelectedArea(player);
                player.sendMessage(new TextComponentString("Reset selected area"));
            }
        }

        return super.onItemRightClick(world, player, hand);
    }

    private void resetSelectedArea(EntityPlayer player)
    {
        ItemStack stack = player.getHeldItemMainhand();
        NBTTagCompound tag = stack.getTagCompound();

        if (tag != null)
        {
            //create a new tag with only the conduit data (i.e. not the first/second positions)
            NBTTagCompound newTag = new NBTTagCompound();
            if(tag.hasKey(NBT_CONDUIT_DATA_KEY))
                newTag.setTag(NBT_CONDUIT_DATA_KEY, tag.getTag(NBT_CONDUIT_DATA_KEY));

            if(tag.hasKey(NBT_CONDUIT_DIRECTION_KEY))
                newTag.setTag(NBT_CONDUIT_DIRECTION_KEY, tag.getTag(NBT_CONDUIT_DIRECTION_KEY));

            //overwrite the old tag
            stack.setTagCompound(newTag);
        }
    }

    @Override
    public EnumActionResult onItemUseFirst(EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand)
    {
        if (hand == EnumHand.OFF_HAND)
            return EnumActionResult.PASS;

        if (!world.isRemote)
        {
            ItemStack stack = player.getHeldItemMainhand();
            ItemMode mode = getItemMode(stack);

            if (mode == ItemMode.COPY)
                return copySettings(player, world, pos);
            else
                return pasteSettings(player, world, pos, side);
        }
        else
        {
            //todo handle rendering the box preview overlay thing to show what area is currently selected
        }

        return EnumActionResult.PASS;
    }

    /**
     * Handle the item being used by a player, when in copy mode
     *
     * @param player The player that used the item
     * @param world  The world the player is in
     * @param pos    The position of the block that the item was used on
     * @return
     */
    private EnumActionResult copySettings(EntityPlayer player, World world, BlockPos pos)
    {
        IBlockState blockState = world.getBlockState(pos);
        Block block = blockState.getBlock();
        TileEntity tileEntity = world.getTileEntity(pos);

        if (block instanceof BlockConduitBundle && tileEntity != null)
        {
            BlockConduitBundle bundle = (BlockConduitBundle) block;
            RaytraceResult result = bundle.doRayTrace(world, pos, player);
            CollidableComponent component = result.component;

            if (!component.isCore())
            {
                ItemStack itemStack = player.getHeldItemMainhand();
                NBTTagCompound tag = new NBTTagCompound();

                if (!(itemStack.getItem() instanceof UberProbeItem))
                {
                    //Should be impossible.. I think
                    throw new IllegalStateException("Handler was triggered for item that is not UberProbeItem");
                }

                //For every type of conduit in the bundle (e.g. redstone, item, energy, etc)
                for (IServerConduit conduit : ((IConduitBundle) tileEntity).getServerConduits())
                {
                    EnumFacing dir = component.getDirection();
                    //if the type of conduit has a connection in the desired direction
                    if (conduit.getExternalConnections().contains(dir))
                    {
                        //write the info to the tag
                        conduit.writeConnectionSettingsToNBT(dir, tag);
                    }
                }

                if (itemStack.getTagCompound() == null)
                    itemStack.setTagCompound(new NBTTagCompound());

                //put the info into the item
                NBTTagCompound itemTag = itemStack.getTagCompound();
                itemTag.setTag(NBT_CONDUIT_DATA_KEY, tag);
                itemTag.setInteger(NBT_CONDUIT_DIRECTION_KEY, component.getDirection().getIndex());

                //logger.info(tag.toString());

                player.sendMessage(new TextComponentString("Copied conduit configuration"));
                return EnumActionResult.SUCCESS;
            }
        }

        return EnumActionResult.PASS;
    }

    private EnumActionResult pasteSettings(EntityPlayer player, World world, BlockPos pos, EnumFacing side)
    {
        Style redStyle = new Style().setColor(TextFormatting.RED);
        ItemStack itemStack = player.getHeldItemMainhand();

        if (itemStack.getTagCompound() == null)
        {
            player.sendMessage(new TextComponentString("No conduit configuration is stored!"));

            itemStack.setTagCompound(new NBTTagCompound());
            return EnumActionResult.FAIL;
        }

        NBTTagCompound tag = itemStack.getTagCompound();

        if (tag.hasKey(NBT_CONDUIT_DATA_KEY) && tag.hasKey(NBT_CONDUIT_DIRECTION_KEY) && tag.hasKey(NBT_FIRST_POS_KEY))
        {
            EnumFacing desiredDirection = EnumFacing.values()[tag.getInteger(NBT_CONDUIT_DIRECTION_KEY)];
            NBTTagCompound conduitData = tag.getCompoundTag(NBT_CONDUIT_DATA_KEY);
            BlockPos firstPosition = BlockPos.fromLong(tag.getLong(NBT_FIRST_POS_KEY));
            long volume = getVolumeBetweenPoints(firstPosition, pos);

            if(volume > getMaxConfigurationVolume())
            {
                player.sendMessage(new TextComponentString("Could not configure a volume of " + volume + " blocks. The maximum allowable volume is " + getMaxConfigurationVolume() + " blocks.").setStyle(redStyle));
            }
            else
            {
                player.sendMessage(new TextComponentString("Configured from " + posToHumanFriendlyString(firstPosition) + " to " + posToHumanFriendlyString(pos)));

                configureConduitsInRange(firstPosition, pos, world, desiredDirection, conduitData);
                resetSelectedArea(player);
            }
        }
        else if (!(tag.hasKey(NBT_CONDUIT_DATA_KEY) && tag.hasKey(NBT_CONDUIT_DIRECTION_KEY)))
        {
            player.sendMessage(new TextComponentString("No conduit configuration is stored!").setStyle(redStyle));

            return EnumActionResult.FAIL;
        }
        else
        {
            itemStack.getTagCompound().setLong(NBT_FIRST_POS_KEY, pos.toLong());

            player.sendMessage(new TextComponentString("Configuring from " + posToHumanFriendlyString(pos)));
        }

        return EnumActionResult.SUCCESS;
    }

    private void configureConduitsInRange(BlockPos firstPosition, BlockPos secondPosition, World world, EnumFacing desiredDirection, NBTTagCompound conduitData)
    {
        for (int x = Math.min(secondPosition.getX(), firstPosition.getX()); x <= Math.max(secondPosition.getX(), firstPosition.getX()); x++)
        {
            for (int y = Math.min(secondPosition.getY(), firstPosition.getY()); y <= Math.max(secondPosition.getY(), firstPosition.getY()); y++)
            {
                for (int z = Math.min(secondPosition.getZ(), firstPosition.getZ()); z <= Math.max(secondPosition.getZ(), firstPosition.getZ()); z++)
                {
                    BlockPos breakPos = new BlockPos(x, y, z);
                    IBlockState blockState = world.getBlockState(breakPos);
                    Block block = blockState.getBlock();
                    TileEntity tileEntity = world.getTileEntity(breakPos);

                    if (block instanceof BlockConduitBundle && tileEntity instanceof IConduitBundle)
                    {
                        for (IServerConduit conduit : ((IConduitBundle) tileEntity).getServerConduits())
                        {
                            conduit.readConduitSettingsFromNBT(desiredDirection, conduitData);
                        }
                    }
                }
            }
        }
    }

    private long getVolumeBetweenPoints(BlockPos a, BlockPos b)
    {
        int dx = Math.abs(Math.max(a.getX(), b.getX()) - Math.min(a.getX(), b.getX()));
        int dy = Math.abs(Math.max(a.getY(), b.getY()) - Math.min(a.getY(), b.getY()));
        int dz = Math.abs(Math.max(a.getZ(), b.getZ()) - Math.min(a.getZ(), b.getZ()));

        return (long) dx * (long) dy * (long) dz;
    }

    private String posToHumanFriendlyString(BlockPos pos)
    {
        return "(" + pos.getX() + ", " + pos.getY() + ", " + pos.getZ() + ")";
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn)
    {
        if (!isShiftDown())
        {
            SpecialTooltipHandler.addShowDetailsTooltip(tooltip);
        }
        else
        {
            tooltip.add("In copy mode, click on a conduit to copy its configuration.");
            tooltip.add("Note that only connections with the same direction as the copied connection will be affected by a paste.");
            tooltip.add("In paste mode, click to select 2 positions. All conduits with the same direction as the copied one will be configured.");
            tooltip.add("A box will show the selected area.");
            tooltip.add(Minecraft.getMinecraft().gameSettings.keyBindUseItem.getDisplayName() + " in air to clear the selected area.");
            tooltip.add("Use Shift-Mouse wheel or '" + KeyTracker.yetawrenchmode.getBinding().getDisplayName() + "' to change mode");
        }
    }

    public void registerModel()
    {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "variant=copy"));
        ModelLoader.setCustomModelResourceLocation(this, 1, new ModelResourceLocation(getRegistryName(), "variant=paste"));
    }

    @Override
    public boolean shouldHideFacades(@Nonnull ItemStack itemStack, @Nonnull EntityPlayer entityPlayer)
    {
        return true;
    }

    public enum ItemMode
    {
        COPY, PASTE
    }
}
