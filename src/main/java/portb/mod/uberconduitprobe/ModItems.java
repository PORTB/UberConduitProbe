package portb.mod.uberconduitprobe;

import net.minecraftforge.fml.common.registry.GameRegistry;
import portb.mod.uberconduitprobe.item.UberProbeItem;

public class ModItems
{
    @GameRegistry.ObjectHolder(UberConduitProbe.MODID + ":" + UberProbeItem.REGISTRY_NAME)
    public static UberProbeItem uberProbeItem;
}
