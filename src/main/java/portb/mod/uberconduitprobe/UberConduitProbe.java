package portb.mod.uberconduitprobe;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import org.apache.logging.log4j.Logger;
import portb.mod.uberconduitprobe.network.PacketProbeChangeMode;
import portb.mod.uberconduitprobe.proxy.CommonProxy;

@Mod(name = UberConduitProbe.NAME, version = UberConduitProbe.VERSION, modid = UberConduitProbe.MODID
, dependencies = "required-after:enderio@[5.2.60,)")
public class UberConduitProbe
{
    public static final String MODID = "@MOD_ID@";
    public static final String NAME = "@MOD_NAME@";
    public static final String VERSION = "@MOD_VERSION@";
    public static final SimpleNetworkWrapper network = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);
    @SidedProxy(clientSide = "portb.mod.uberconduitprobe.proxy.ClientProxy", serverSide = "portb.mod.uberconduitprobe.proxy.CommonProxy")
    public static CommonProxy proxy;

    @Mod.Instance
    public static UberConduitProbe INSTANCE;
    private static Logger logger;
    private static int next_id = 0;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        logger = event.getModLog();
        proxy.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        proxy.init(event);
        network.registerMessage(PacketProbeChangeMode.Handler.class, PacketProbeChangeMode.class, next_id++, Side.SERVER);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
        proxy.postInit(event);
    }

}
