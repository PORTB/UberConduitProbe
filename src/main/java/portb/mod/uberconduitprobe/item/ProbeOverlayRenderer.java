package portb.mod.uberconduitprobe.item;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.lwjgl.opengl.GL11;
import portb.mod.uberconduitprobe.IconUCP;
import portb.mod.uberconduitprobe.UberConduitProbe;

import javax.annotation.Nonnull;

@Mod.EventBusSubscriber(modid = UberConduitProbe.MODID, value = Side.CLIENT)
public class ProbeOverlayRenderer
{
    @SubscribeEvent
    public static void renderOverlay(@Nonnull RenderGameOverlayEvent.Post event)
    {
        if (event.getType() == RenderGameOverlayEvent.ElementType.ALL)
        {
            ItemStack probeItem = getEquippedProbe();

            if (probeItem != ItemStack.EMPTY)
            {
                doRenderOverlay(event, probeItem);
            }
        }
    }

    private static @Nonnull
    ItemStack getEquippedProbe()
    {
        ItemStack equipped = Minecraft.getMinecraft().player.getHeldItemMainhand();

        if (equipped.getItem() instanceof UberProbeItem)
        {
            return equipped;
        }

        return ItemStack.EMPTY;
    }

    private static void doRenderOverlay(RenderGameOverlayEvent event, @Nonnull ItemStack equippedProbe)
    {
        IconUCP icon1, icon2;
        if (equippedProbe.getItemDamage() == 0)
        {
            icon1 = IconUCP.PROBE_OVERLAY_COPY_ON;
            icon2 = IconUCP.PROBE_OVERLAY_PASTE_OFF;
        }
        else
        {
            icon1 = IconUCP.PROBE_OVERLAY_COPY_OFF;
            icon2 = IconUCP.PROBE_OVERLAY_PASTE_ON;
        }
        ScaledResolution res = event.getResolution();

        double offsetX = res.getScaledWidth() - 48;
        double offsetY = res.getScaledHeight() - 16;
        GL11.glColor4f(1, 1, 1, 0.75f);
        icon1.getMap().render(icon1, offsetX - 32, offsetY - 32, 32, 32, 0, true);
        icon2.getMap().render(icon2, offsetX, offsetY - 32, 32, 32, 0, true);
    }
}
