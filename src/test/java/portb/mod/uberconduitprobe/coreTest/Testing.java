package portb.mod.uberconduitprobe.coreTest;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

public class Testing
{
    @Test
    void thingy()
    {
        Set<BlockPos> coordinates = makeSetForBlockPosRange(new BlockPos(2, 2, 2), new BlockPos(10, 2, 10));

        Vec3d playerPos = new Vec3d(5, 3, 5);

        //Will this even work??
        Set<BlockPos> sorted = coordinates.stream().sorted((o1, o2) ->
        {
            Vec3d a = new Vec3d(o1.getX(), o1.getY(), o1.getZ());
            Vec3d b = new Vec3d(o2.getX(), o2.getY(), o2.getZ());

            double aDist = a.distanceTo(playerPos);
            double bDist = b.distanceTo(playerPos);

            if(aDist < bDist)
            {
                return -1;
            }
            else if(aDist == bDist)
            {
                return 0;
            }
            else if(aDist > bDist)
            {
                return 1;
            }

            return 0; //??? should never happen
        }).collect(Collectors.toCollection(LinkedHashSet::new));

        for(BlockPos pos : sorted)
        {
            Vec3d relativeDist = new Vec3d(pos.getX(), pos.getY(), pos.getZ());

            System.out.println(pos.toString() + " mod = " + relativeDist.subtract(playerPos).lengthSquared());
        }
    }

    static private Set<BlockPos> makeSetForBlockPosRange(BlockPos start, BlockPos end)
    {
        List<BlockPos> posList = new ArrayList<>();

        for (int x = Math.min(start.getX(), end.getX()); x <= Math.max(start.getX(), end.getX()); x++)
        {
            for (int y = Math.min(start.getY(), end.getY()); y <= Math.max(start.getY(), end.getY()); y++)
            {
                for (int z = Math.min(start.getZ(), end.getZ()); z <= Math.max(start.getZ(), end.getZ()); z++)
                {
                    posList.add(new BlockPos(x, y, z));
                }
            }
        }

        return new HashSet<>(posList);
    }
}
