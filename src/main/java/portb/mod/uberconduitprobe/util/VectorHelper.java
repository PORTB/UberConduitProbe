package portb.mod.uberconduitprobe.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;

public class VectorHelper
{
    /**
     * Raytraces where the player is looking
     *
     * @param playerIn the player
     * @return where the player is looking
     */
    public static RayTraceResult getPlayerLookingAt(EntityPlayer playerIn)
    {
        double reachDistance = playerIn.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue();
        Vec3d look = playerIn.getLookVec();
        Vec3d start = new Vec3d(playerIn.posX, playerIn.posY + playerIn.getEyeHeight(), playerIn.posZ);
        Vec3d end = new Vec3d(
                playerIn.posX + look.x * reachDistance,
                playerIn.posY + look.y * reachDistance + playerIn.getEyeHeight(),
                playerIn.posZ + look.z * reachDistance);

        return playerIn.world.rayTraceBlocks(start, end, false, true, false);
    }
}
