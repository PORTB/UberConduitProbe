package portb.mod.uberconduitprobe.proxy;

import crazypants.enderio.base.handler.KeyTracker;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.event.MouseEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.relauncher.Side;
import portb.mod.uberconduitprobe.ModItems;
import portb.mod.uberconduitprobe.UberConduitProbe;
import portb.mod.uberconduitprobe.item.UberProbeItem;
import portb.mod.uberconduitprobe.network.PacketProbeChangeMode;
import portb.mod.uberconduitprobe.util.RendererHelper;
import portb.mod.uberconduitprobe.util.VectorHelper;

@Mod.EventBusSubscriber(Side.CLIENT)
public class ClientProxy extends CommonProxy
{
    @SubscribeEvent
    public static void onModelRegister(ModelRegistryEvent event)
    {
        ModItems.uberProbeItem.registerModel();

        //ModItems.uberProbeItem.registerRenderers(event.g);
    }

    @SubscribeEvent
    public static void onMouseEvent(MouseEvent event)
    {
        EntityPlayerSP player = Minecraft.getMinecraft().player;
        ItemStack stack = player.getHeldItemMainhand();
        Item item = stack.getItem();

        if (item instanceof UberProbeItem)
        {
            if (event.getDwheel() != 0 && player.isSneaking())
            {
                toggleProbeMode(stack);
                event.setCanceled(true);
            }
        }
    }

    @SubscribeEvent
    public static void onKeyInput(InputEvent.KeyInputEvent event)
    {
        EntityPlayerSP player = Minecraft.getMinecraft().player;
        ItemStack stack = player.getHeldItemMainhand();
        Item item = stack.getItem();
        if (item instanceof UberProbeItem)
        {
            if (KeyTracker.yetawrenchmode.getBinding().isPressed())
            {
                toggleProbeMode(stack);
                player.swingArm(EnumHand.MAIN_HAND);
            }
        }
    }

    private static void toggleProbeMode(ItemStack stack)
    {
        int newDamage = stack.getItemDamage() == 0 ? 1 : 0;
        stack.setItemDamage(newDamage);

        UberConduitProbe.network.sendToServer(new PacketProbeChangeMode());
    }

    @SubscribeEvent
    public static void renderWorldLastEvent(RenderWorldLastEvent event)
    {
        Minecraft mc = Minecraft.getMinecraft();
        EntityPlayer player = mc.player;
        ItemStack stack = player.getHeldItemMainhand();

        //RenderingBetter.renderMyThing(event, player, new BlockPos(0, 20, 0), new BlockPos(-10,10,-10), (byte)255, (byte)0, (byte)0);
        //RendererHelper.renderSelectedAreaOverlay(event, player, new BlockPos(0, 20, 0), new BlockPos(10,10,-10), (byte)0, (byte)0, (byte)255);
        //RendererHelper.renderSelectedAreaOverlay(event, player, new BlockPos(0, 20, 0), new BlockPos(-10,10,10), (byte)255, (byte)255, (byte)255);
        //RenderingBetter.renderMyThing(event, player, new BlockPos(0, 20, 0), new BlockPos(10,10,10), (byte)0, (byte)255, (byte)0);

        //RendererHelper.renderSelectedAreaOverlay(event, player, new BlockPos(0, 20, 0), new BlockPos(-10,30,-10), (byte)0, (byte)0, (byte)255);
        //RendererHelper.renderSelectedAreaOverlay(event, player, new BlockPos(0, 20, 0), new BlockPos(10,30,-10), (byte)255, (byte)0, (byte)0);
        //RenderingBetter.renderMyThing(event, player, new BlockPos(0, 20, 0), new BlockPos(-10,30,10), (byte)0, (byte)255, (byte)0);
        //RendererHelper.renderSelectedAreaOverlay(event, player, new BlockPos(0, 20, 0), new BlockPos(10,30,10),  (byte)255, (byte)255, (byte)255);

        //RendererHelper.renderSelectedAreaOverlay(event, player, new BlockPos(1,8,62), new BlockPos(6, 4, 58), (byte)0, (byte)0, (byte)0);

        if (stack.getItem() instanceof UberProbeItem)
            if (UberProbeItem.getItemMode(stack) == UberProbeItem.ItemMode.PASTE)
                if (UberProbeItem.isSelecting(stack))
                {
                    RayTraceResult rayTraceResult = VectorHelper.getPlayerLookingAt(player);

                    if (rayTraceResult != null)
                    {
                        BlockPos firstPosition = UberProbeItem.getFirstPosition(stack);
                        BlockPos secondPosition = rayTraceResult.getBlockPos();
                        RendererHelper.renderSelectedAreaOverlay(event, player, firstPosition, secondPosition, (byte) 255, (byte) 30, (byte) 0);
                    }
                }
    }

    @Override
    public void registerItemRenderer(Item item, int meta, String id)
    {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(item.getRegistryName(), id));
    }


}
