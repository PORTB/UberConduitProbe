package portb.mod.uberconduitprobe.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import portb.mod.uberconduitprobe.item.UberProbeItem;

public class PacketProbeChangeMode implements IMessage
{
    //region unused overrides
    @Override
    public void fromBytes(ByteBuf buf)
    {

    }

    @Override
    public void toBytes(ByteBuf buf)
    {

    }
    //endregion

    public static class Handler implements IMessageHandler<PacketProbeChangeMode, IMessage>
    {

        @Override
        public IMessage onMessage(PacketProbeChangeMode message, MessageContext ctx)
        {
            if (!changeModeForItemStack(ctx.getServerHandler().player.getHeldItemMainhand()))
                changeModeForItemStack(ctx.getServerHandler().player.getHeldItemOffhand());

            return null;
        }

        private boolean changeModeForItemStack(ItemStack stack)
        {
            if (stack.getItem() instanceof UberProbeItem)
            {
                int newDamage = stack.getItemDamage() == 0 ? 1 : 0;
                stack.setItemDamage(newDamage);
                return true;
            }

            return false;
        }
    }


}
