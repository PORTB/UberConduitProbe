package portb.mod.uberconduitprobe;

import com.enderio.core.api.client.render.IWidgetIcon;
import com.enderio.core.api.client.render.IWidgetMap;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public enum IconUCP implements IWidgetIcon
{
    PROBE_OVERLAY_PASTE_ON(32, 0, 32, 32),
    PROBE_OVERLAY_PASTE_OFF(0, 0, 32, 32),
    PROBE_OVERLAY_COPY_ON(32, 32, 32, 32),
    PROBE_OVERLAY_COPY_OFF(0, 32, 32, 32);

    public static final @Nonnull
    ResourceLocation TEXTURE = UberConduitProbe.proxy.getGuiTexture("gui/copy_paste_icons");
    //this region may look the same as IconEIO... because it is the same
    //region
    private static final int TEX_SIZE = 64;
    public static final @Nonnull
    IWidgetMap map = new IWidgetMap.WidgetMapImpl(TEX_SIZE, TEXTURE)
    {
        @Override
        @SideOnly(Side.CLIENT)
        public void render(@Nonnull IWidgetIcon widget, double x, double y, double width, double height, double zLevel, boolean doDraw, boolean flipY)
        {
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            super.render(widget, x, y, width, height, zLevel, doDraw, flipY);
        }
    };
    public final int x;
    public final int y;
    public final int width;
    public final int height;
    public final @Nullable
    IconUCP overlay;

    IconUCP(int x, int y)
    {
        this(x, y, 16, 16, null);
    }

    IconUCP(int x, int y, @Nonnull IconUCP overlay)
    {
        this(x, y, 16, 16, overlay);
    }

    IconUCP(int x, int y, int width, int height)
    {
        this(x, y, width, height, null);
    }

    IconUCP(int x, int y, int width, int height, @Nullable IconUCP overlay)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.overlay = overlay;
    }

    @Override
    public @Nonnull
    IWidgetMap getMap()
    {
        return map;
    }

    @Override
    public int getX()
    {
        return x;
    }

    @Override
    public int getY()
    {
        return y;
    }

    @Override
    public int getWidth()
    {
        return width;
    }

    @Override
    public int getHeight()
    {
        return height;
    }

    @Override
    public @Nullable
    IconUCP getOverlay()
    {
        return overlay;
    }
    //endregion
}
