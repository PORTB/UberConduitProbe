package portb.mod.uberconduitprobe.proxy;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import portb.mod.uberconduitprobe.UberConduitProbe;
import portb.mod.uberconduitprobe.item.UberProbeItem;

import javax.annotation.Nonnull;

@Mod.EventBusSubscriber
public class CommonProxy
{

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
    {

    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
    {
        event.getRegistry().register(new UberProbeItem());
    }

    public void preInit(FMLPreInitializationEvent event)
    {
    }

    public void init(FMLInitializationEvent event)
    {
    }

    public void postInit(FMLPostInitializationEvent event)
    {
    }

    @SubscribeEvent
    public void registerRecipes(RegistryEvent.Register<IRecipe> event)
    {

    }

    public void registerItemRenderer(Item item, int meta, String id)
    {

    }

    @Nonnull
    public ResourceLocation getGuiTexture(@Nonnull String name)
    {
        return new ResourceLocation(UberConduitProbe.MODID, "textures/" + name + ".png");
    }
}
